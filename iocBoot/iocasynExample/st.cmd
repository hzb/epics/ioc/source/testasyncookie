#!../../bin/linux-x86_64/asynExample

< envPaths
epicsEnvSet("ENGINEER","Will Smith")

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/asynExample.dbd"
asynExample_registerRecordDeviceDriver pdbbase

## Load record instances

#var streamDebug 1

cd "${TOP}/iocBoot/${IOC}"
iocInit

dbl > /opt/epics/ioc/log/logs.dbl
